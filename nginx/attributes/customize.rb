# disable file size checking
normal[:nginx][:client_max_body_size] = 0
